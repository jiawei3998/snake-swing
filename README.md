# 贪吃蛇小游戏

使用的是 JDK1.8, 环境地址: https://blog.csdn.net/weixin_44953227/article/details/109247665


## 已经编译的jar包

当前目录下的 `snake-swing.jar`

### 游戏主启动类

```bash
package com.snake.GamePanel;
```

### 游戏面板类

```bash
package com.snake.Data;
```

### 游戏数据类

```bash
package com.snake.Data;
```

### 游戏静态资源
资源在 static 目录下
```bash
路径: src/static
```